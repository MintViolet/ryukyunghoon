using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(BoxCollider2D))]
public class GameEvent : MonoBehaviour
{
    public string activeType;
    public string activeOption;
    public float activeValue;

    public string eventLoopType;
    public float eventLoopPeriod;
    public string eventType;
    public string eventOption;
    public float eventValue;

    public event Action<Collider2D> onTriggerEnterEvent;
	public event Action<Collider2D> onTriggerStayEvent;
	public event Action<Collider2D> onTriggerExitEvent;
}
