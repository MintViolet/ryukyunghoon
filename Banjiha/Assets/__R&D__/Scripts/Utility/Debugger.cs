﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debugger : MonoBehaviour
{
    public static void PrintLog(string log, LogType type)
    {
        if (type == LogType.Log)
        {
            Debug.Log(log);
        }
        else if (type == LogType.Error)
        {
            Debug.LogError(log);
        }
        else
        {
            Debug.Log(log);
        }
    }
}
