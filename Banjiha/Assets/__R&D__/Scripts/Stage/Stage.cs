﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage : MonoBehaviour
{
    private GameEvent[] _stageEvents;

    public Vector2 startPos;

    public void GetGameEvents()
    {
        _stageEvents = FindObjectsOfType<GameEvent>();    
    }

    public void SetPlayerPostionToStartPosition()
    {
        StageManager.instance.GetPlayerController().transform.position = startPos;
    }
}
