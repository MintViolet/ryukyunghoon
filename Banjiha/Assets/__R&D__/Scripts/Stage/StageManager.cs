﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageManager : Singleton<StageManager>
{
    private int _currentStageIndex;
    private CharacterController2D _controller;

    public Stage[] stages;

    private void Start()
    {
        _controller = FindObjectOfType<CharacterController2D>();

        ActiveStage(0);
    }

    public void ActiveStage(int idx)
    {
        stages[_currentStageIndex].gameObject.SetActive(false);

        _currentStageIndex = idx;

        stages[_currentStageIndex].gameObject.SetActive(true);

        stages[_currentStageIndex].SetPlayerPostionToStartPosition();
        stages[_currentStageIndex].GetGameEvents();
    }

    public void ActiveNextStage()
    {
        ActiveStage(_currentStageIndex + 1);
    }

    public CharacterController2D GetPlayerController()
    {
        return _controller;
    }
}
