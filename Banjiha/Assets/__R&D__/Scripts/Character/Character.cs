﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Character : MonoBehaviour
{
    private const float DEEP_IDLE_WAIT_TIME = 5f;

    private float normalizedHorizontalSpeed = 0;

    private CharacterController2D _controller;
    private Animator _animator;
    private RaycastHit2D _lastControllerColliderHit;
    private Vector3 _velocity;
    private int _jumpCount;
    private float _deepIdleTimeCount;

    private CharacterState _characterState;

    public enum CharacterState
    {
        STATE_IDLE = 0,
        STATE_DEEP_IDLE,
        STATE_RUN,
        STATE_JUMP,
    };

    public float gravity = -25f;
    public float runSpeed = 8f;
    public float groundDamping = 20f;
    public float inAirDamping = 5f;
    public float jumpHeight = 0f;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _controller = GetComponent<CharacterController2D>();

        _controller.onTriggerEnterEvent += OnTriggerEnterEvent;
        _controller.onTriggerExitEvent += OnTriggerExitEvent;
    }

    private void Update()
    {
        if (_controller.isGrounded)
        {
            _velocity.y = 0;
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            normalizedHorizontalSpeed = 1;

            if (transform.localScale.x < 0f)
            {
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
            }

            if (_controller.isGrounded)
            {
                SetCharacterState(CharacterState.STATE_RUN);
            }
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            normalizedHorizontalSpeed = -1;

            if (transform.localScale.x > 0f)
            {
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
            }

            if (_controller.isGrounded)
            {
                SetCharacterState(CharacterState.STATE_RUN);
            }
        }
        else
        {
            normalizedHorizontalSpeed = 0;

            if (_controller.isGrounded)
            {
                if (GetCharacterState() != CharacterState.STATE_DEEP_IDLE)
                {
                    SetCharacterState(CharacterState.STATE_IDLE);

                    _deepIdleTimeCount += Time.deltaTime;
                    if (_deepIdleTimeCount >= DEEP_IDLE_WAIT_TIME)
                    {
                        SetCharacterState(CharacterState.STATE_DEEP_IDLE);
                    }
                }
            }
        }

        if (Input.GetKey(KeyCode.Space))
        {
            if (_jumpCount <= 5)
            {
                _jumpCount++;
                _velocity.y += jumpHeight / (_jumpCount + 1);
                SetCharacterState(CharacterState.STATE_JUMP);
            }
        }
        else
        {
            if (_controller.isGrounded)
            {
                _jumpCount = 0;
            }
        }

        float smoothedMovementFactor = _controller.isGrounded ? groundDamping : inAirDamping;
        _velocity.x = Mathf.Lerp(_velocity.x, normalizedHorizontalSpeed * runSpeed, Time.deltaTime * smoothedMovementFactor);

        _velocity.y += gravity * Time.deltaTime;

        if (_controller.isGrounded && Input.GetKey(KeyCode.DownArrow))
        {
            if (GetCharacterState() != CharacterState.STATE_JUMP)
            {
                _velocity.y *= 15f;
                _controller.ignoreOneWayPlatformsThisFrame = true;
            }
        }

        _controller.move(_velocity * Time.deltaTime);
        _velocity = _controller.velocity;
    }

    public void SetCharacterState(CharacterState state)
    {
        if (GetCharacterState() != state)
        {
            _characterState = state;
            ProcessState();

            _deepIdleTimeCount = 0;
        }
    }

    public CharacterState GetCharacterState()
    {
        return _characterState;
    }

    void ProcessState()
    {
        switch (_characterState)
        {
            case CharacterState.STATE_IDLE:
                _animator.Play(Animator.StringToHash("Idle"));
                break;

            case CharacterState.STATE_DEEP_IDLE:
                _animator.Play(Animator.StringToHash("DeepIdle"));
                Invoke("SetDeepIdle", 0.417f * 0.5f);
                break;

            case CharacterState.STATE_RUN:
                _animator.Play(Animator.StringToHash("Walk"));
                break;

            case CharacterState.STATE_JUMP:
                _animator.Play(Animator.StringToHash("Jump"));
                break;
        }
    }

    void SetDeepIdle()
    {
        _animator.Play(Animator.StringToHash("DeepIdleLooping"));
    }

    void OnTriggerEnterEvent(Collider2D col)
    {
        Debug.Log("onTriggerEnterEvent: " + col.gameObject.name);
    }

    void OnTriggerExitEvent(Collider2D col)
    {
        Debug.Log("onTriggerExitEvent: " + col.gameObject.name);
    }
}
